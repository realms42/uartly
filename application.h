#ifndef APPLICATION_H
#define APPLICATION_H

#include <QApplication>
#include <QSerialPort>
#include <QStringListModel>
#include <QTimer>
#include <QObject>

#include "gui.h"

class Application : public QObject
{
    Q_OBJECT
public:
    Application(int, char*[]);
    int run();
    void openConnection(bool);
private:
    QApplication qApplication;
    Gui gui;
    QSerialPort activeSerialPort;
    QTimer devicePollTimer;
    void refreshPorts();
    void qConnect();
};

#endif // APPLICATION_H
