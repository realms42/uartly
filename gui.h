#ifndef GUI_H
#define GUI_H

#include <QApplication>
#include <QMainWindow>
#include <QStringListModel>
#include <QTimer>

#include "gui/serialsettingsview.h"
#include "gui/inputoutputview.h"
#include "gui/inputsettingsview.h"
#include "gui/sendview.h"

class Application;

class Gui : public QMainWindow
{
public:
    Gui(Application&);
    static void setTheme();
    void setSerialPortList(const QStringList &, const QString &);
    void setBaudrateList(const QStringList &, const QString &);
    void setDataBitsList(const QStringList &, const QString &);
    void setParityList(const QStringList &, const QString &);
    void setFlowControlList(const QStringList &, const QString &);
    void setStopBitsList(const QStringList &, const QString &);
    QStringList getSerialPortList() const;
    QString getCurrentSerialPort() const;
private:

    SerialSettingsView serialSettingsView;
    InputOutputView inputOutputView;
    InputSettingsView inputSettingsView;
    SendView sendView;

    QStringListModel serialPortModel;
    QStringListModel baudrateModel;
    QStringListModel dataBitsModel;
    QStringListModel parityModel;
    QStringListModel stopBitsModel;
    QStringListModel flowControlModel;

    QTimer serialPortMonitor;

    void generateLayout();
};

#endif // GUI_H
