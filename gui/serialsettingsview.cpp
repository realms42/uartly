#include "serialsettingsview.h"
#include "application.h"
#include <QObject>
#include <QLayout>
#include <QLabel>
#include <QPushButton>
#include <QComboBox>
#include <QApplication>
#include <QSerialPortInfo>

SerialSettingsView::SerialSettingsView(Application &app) :
    application(app)
{
    addLayout(&serialSettingsLayout);
    serialSettingsLayout.addWidget(&serialPortComboBox);
    serialSettingsLayout.addWidget(&baudrateComboBox);
    connectButton.setText(QStringLiteral("Connect"));
    serialSettingsLayout.addWidget(&connectButton);

    addLayout(&extraSettings);
    dataBitsLabel.setText(QStringLiteral("Data bits: "));
    extraSettings.addWidget(&dataBitsLabel);
    extraSettings.addWidget(&dataBitsComboBox);
    parityLabel.setText(QStringLiteral("Parity: "));
    extraSettings.addWidget(&parityLabel);
    extraSettings.addWidget(&parityComboBox);
    stopBitsLabel.setText(QStringLiteral("Stop bits: "));
    extraSettings.addWidget(&stopBitsLabel);
    extraSettings.addWidget(&stopBitsComboBox);
    flowControlLabel.setText(QStringLiteral("Flow control: "));
    extraSettings.addWidget(&flowControlLabel);
    extraSettings.addWidget(&flowControlComboBox);

    qConnect();
}

SerialSettingsView *SerialSettingsView::setSerialPortModel(QStringListModel *model)
{
    serialPortComboBox.setModel(model);
    return this;
}

SerialSettingsView *SerialSettingsView::setBaudrateModel(QStringListModel *model)
{
    baudrateComboBox.setModel(model);
    return this;
}

SerialSettingsView *SerialSettingsView::setDataBitsModel(QStringListModel *model)
{
    dataBitsComboBox.setModel(model);
    return this;
}

SerialSettingsView *SerialSettingsView::setParityBitsModel(QStringListModel *model)
{
    parityComboBox.setModel(model);
    return this;
}

SerialSettingsView *SerialSettingsView::setStopBitsModel(QStringListModel *model)
{
    stopBitsComboBox.setModel(model);
    return this;
}

SerialSettingsView *SerialSettingsView::setFlowControlModel(QStringListModel *model)
{
    flowControlComboBox.setModel(model);
    return this;
}

void SerialSettingsView::setSerialPortText(const QString &text)
{
    if(serialPortComboBox.findText(text) != -1){
        serialPortComboBox.setCurrentText(text);
    }else{
        if(serialPortComboBox.count())
            serialPortComboBox.setCurrentIndex(0);
    }
}

QString SerialSettingsView::getSerialPortText() const
{
    return serialPortComboBox.currentText();
}

void SerialSettingsView::setBaudrateText(const QString &text)
{
    baudrateComboBox.setCurrentText(text);
}

void SerialSettingsView::setDataBitsText(const QString &text)
{
    dataBitsComboBox.setCurrentText(text);
}

void SerialSettingsView::setParityBitsText(const QString &text)
{
    parityComboBox.setCurrentText(text);
}

void SerialSettingsView::setStopBitsText(const QString &text)
{
    stopBitsComboBox.setCurrentText(text);
}

void SerialSettingsView::setFlowControlText(const QString &text)
{
    flowControlComboBox.setCurrentText(text);
}

void SerialSettingsView::qConnect()
{

}
