#include "sendview.h"
#include <QPushButton>
#include <QLineEdit>

SendView::SendView()
{
    auto *lineEdit = new QLineEdit;
    addWidget(lineEdit);

    auto *sendButton = new QPushButton("Send");
    addWidget(sendButton);
}
