#ifndef SERIALSETTINGSVIEW_H
#define SERIALSETTINGSVIEW_H

#include <QObject>
#include <QLabel>
#include <QPushButton>
#include <QStringListModel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QComboBox>

class Application;

class SerialSettingsView : public QVBoxLayout
{
    Q_OBJECT
public:
    SerialSettingsView(Application&);
    SerialSettingsView *setSerialPortModel(QStringListModel *model);
    SerialSettingsView *setBaudrateModel(QStringListModel *model);
    SerialSettingsView *setDataBitsModel(QStringListModel *model);
    SerialSettingsView *setParityBitsModel(QStringListModel *model);
    SerialSettingsView *setStopBitsModel(QStringListModel *model);
    SerialSettingsView *setFlowControlModel(QStringListModel *model);
    void setSerialPortText(const QString &);
    QString getSerialPortText() const;
    void setBaudrateText(const QString &);
    void setDataBitsText(const QString &);
    void setParityBitsText(const QString &);
    void setStopBitsText(const QString &);
    void setFlowControlText(const QString &);

private:
    void qConnect();
    QComboBox serialPortComboBox;
    QComboBox baudrateComboBox;
    QComboBox dataBitsComboBox;
    QComboBox parityComboBox;
    QComboBox stopBitsComboBox;
    QComboBox flowControlComboBox;

    QHBoxLayout serialSettingsLayout;
    QHBoxLayout extraSettings;

    QPushButton connectButton;
    QLabel dataBitsLabel;
    QLabel parityLabel;
    QLabel stopBitsLabel;
    QLabel flowControlLabel;

    Application &application;
};

#endif // SERIALSETTINGSVIEW_H
