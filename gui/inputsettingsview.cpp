#include "inputsettingsview.h"
#include <QPushButton>

InputSettingsView::InputSettingsView()
{
    QPushButton *carriageReturnButton = new QPushButton("CR");
    carriageReturnButton->setCheckable(true);
    addWidget(carriageReturnButton);

    QPushButton *lineFeedButton = new QPushButton("LF");
    lineFeedButton->setCheckable(true);
    addWidget(lineFeedButton);

    QPushButton *byteModeButton = new QPushButton("Byte mode");
    byteModeButton->setCheckable(true);
    addWidget(byteModeButton);
}
