#include "inputoutputview.h"
#include <QLayout>
#include <QLabel>
#include <QPushButton>
#include <QPlainTextEdit>
#include <QTableWidget>
#include <QSplitter>
#include <QScrollBar>

InputOutputView::InputOutputView(QWidget *parent) : QVBoxLayout(parent)
{    
    auto *splitter = new QSplitter;
    addWidget(splitter);

    auto *outputText = new QPlainTextEdit;
    outputText->setReadOnly(true);
    outputText->addScrollBarWidget(new QScrollBar, Qt::AlignRight);
    splitter->addWidget(outputText);

    auto  *outputHex = new QPlainTextEdit;
    outputHex->setReadOnly(true);
    outputHex->addScrollBarWidget(new QScrollBar, Qt::AlignRight);
    splitter->addWidget(outputHex);
}
