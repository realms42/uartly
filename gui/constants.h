#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <QStringList>
#include <QString>
#include <QSerialPort>

namespace gui {
static QStringList dataBitsList{QString::number(QSerialPort::Data5),
            QString::number(QSerialPort::Data6),
            QString::number(QSerialPort::Data7),
            QString::number(QSerialPort::Data8)
                               };
static QStringList parityList{QStringLiteral("No parity"),
            QStringLiteral("Even parity"),
            QStringLiteral("Odd parity"),
            QStringLiteral("Space parity"),
            QStringLiteral("Mark parity")
                             };
static QStringList stopBitsList{QStringLiteral("1"),
            QStringLiteral("1.5"),
            QStringLiteral("2")
                               };

static QStringList flowControlList{QStringLiteral("Off"),
            QStringLiteral("RTS/CTS"),
            QStringLiteral("Xon/Xoff")
                           };

}



#endif // CONSTANTS_H
