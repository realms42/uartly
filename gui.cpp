#include "gui.h"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QComboBox>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QTimer>
#include <QSerialPort>
#include <QSerialPortInfo>



Gui::Gui(Application &app) :
    serialSettingsView(app)
{    
    setTheme();
    generateLayout();

    serialSettingsView.setBaudrateModel(&baudrateModel);
    serialSettingsView.setDataBitsModel(&dataBitsModel);
    serialSettingsView.setParityBitsModel(&parityModel);
    serialSettingsView.setStopBitsModel(&stopBitsModel);
    serialSettingsView.setFlowControlModel(&flowControlModel);
    serialSettingsView.setSerialPortModel(&serialPortModel);

    setMinimumSize(640, 360);
    show();
}

void Gui::setTheme(){
    QApplication::setStyle("Fusion");
    QPalette palette;
    palette.setColor(QPalette::Window, QColor(53, 53, 53));
    palette.setColor(QPalette::WindowText, Qt::white);
    palette.setColor(QPalette::Base, QColor(25, 25, 25));
    palette.setColor(QPalette::AlternateBase, QColor(53, 53, 53));
    palette.setColor(QPalette::ToolTipBase, Qt::white);
    palette.setColor(QPalette::ToolTipText, Qt::white);
    palette.setColor(QPalette::PlaceholderText, Qt::white);
    palette.setColor(QPalette::Text, Qt::white);
    palette.setColor(QPalette::Button, QColor(53, 53, 53));
    palette.setColor(QPalette::ButtonText, Qt::white);
    palette.setColor(QPalette::BrightText, Qt::red);

    palette.setColor(QPalette::Link, QColor(42, 130, 218));
    palette.setColor(QPalette::LinkVisited, Qt::magenta);

    palette.setColor(QPalette::Highlight, QColor(42, 130, 218));
    palette.setColor(QPalette::HighlightedText, Qt::black);
    QApplication::setPalette(palette);
}

void Gui::setSerialPortList(const QStringList &list, const QString &def)
{
    serialPortModel.setStringList(list);
    serialSettingsView.setSerialPortText(def);
}

void Gui::setBaudrateList(const QStringList &list, const QString &def)
{
    baudrateModel.setStringList(list);
    serialSettingsView.setBaudrateText(def);
}

void Gui::setDataBitsList(const QStringList &list, const QString &def)
{
    dataBitsModel.setStringList(list);
    serialSettingsView.setDataBitsText(def);
}

void Gui::setParityList(const QStringList &list, const QString &def)
{
    parityModel.setStringList(list);
    serialSettingsView.setParityBitsText(def);
}

void Gui::setFlowControlList(const QStringList &list, const QString &def)
{
    flowControlModel.setStringList(list);
    serialSettingsView.setFlowControlText(def);
}

void Gui::setStopBitsList(const QStringList &list, const QString &def)
{
    stopBitsModel.setStringList(list);
    serialSettingsView.setStopBitsText(def);
}

QStringList Gui::getSerialPortList() const
{
    return serialPortModel.stringList();
}

QString Gui::getCurrentSerialPort() const
{
    return serialSettingsView.getSerialPortText();
}

void Gui::generateLayout()
{
    auto *base = new QWidget;
    auto *mainLayout = new QVBoxLayout;
    setCentralWidget(base);
    base->setLayout(mainLayout);
    mainLayout->addLayout(&serialSettingsView);
    mainLayout->addLayout(&inputOutputView);
    mainLayout->addLayout(&inputSettingsView);
    mainLayout->addLayout(&sendView);
}
